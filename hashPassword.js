const bcrypt = require('bcryptjs');

const password = 'amayas'; // Remplacez par votre mot de passe désiré
const saltRounds = 8;

bcrypt.hash(password, saltRounds, (err, hash) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(`Le hachage pour le mot de passe "${password}" est : ${hash}`);
});
