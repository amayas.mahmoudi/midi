from flask import Flask, Response, request, jsonify
from flask_cors import CORS
import os
import random
import time
import subprocess
from datetime import datetime
import json

app = Flask(__name__)
CORS(app)

# Charger les configurations des playlists depuis un fichier JSON
def load_playlists():
    with open('playlists.json', 'r') as f:
        return json.load(f)

# Sauvegarder les configurations des playlists dans un fichier JSON
def save_playlists(playlists):
    with open('playlists.json', 'w') as f:
        json.dump(playlists, f, indent=4)

# Initialiser les playlists depuis le fichier JSON
PLAYLISTS = load_playlists()

current_playlist_folder = None
songs = []
played_songs = set()
current_song = None
song_start_time = 0
fade_duration = 5  # Durée du fondu en secondes

def get_playlist_folder():
    now = datetime.now()
    day = now.strftime('%a').lower()
    hour = now.hour

    print(f"Jour: {day}, Heure: {hour}")
    
    for playlist, details in PLAYLISTS.items():
        if day in details['days'] and hour in details['hours']:
            print(f"Playlist actuelle : {playlist}")
            return details['folder']
    return None

def load_songs(playlist_folder):
    all_songs = [os.path.join(playlist_folder, song) for song in os.listdir(playlist_folder) if song.endswith('.mp3')]
    random.shuffle(all_songs)
    return all_songs

def get_song_duration(song):
    print(f"Obtention de la durée de la chanson : {song}")
    """Obtient la durée de la chanson en secondes (nécessite ffprobe)"""
    result = subprocess.run(['ffprobe', '-v', 'error', '-show_entries', 'format=duration', '-of', 'default=noprint_wrappers=1:nokey=1', song], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return float(result.stdout)

def get_next_song():
    global played_songs, songs, current_song, song_start_time
    if len(played_songs) == len(songs):
        played_songs.clear()
    
    unplayed_songs = [song for song in songs if song not in played_songs]
    if not unplayed_songs:
        random.shuffle(songs)
        played_songs.clear()
        unplayed_songs = songs
    
    current_song = random.choice(unplayed_songs)
    played_songs.add(current_song)
    song_start_time = time.time()
    
    return current_song

def generate():
    global current_playlist_folder, songs, played_songs, current_song, song_start_time
    while True:
        if current_playlist_folder is not None:
            if current_song is None or (time.time() - song_start_time) > get_song_duration(current_song):
                current_song = get_next_song()
                print(f"Lecture de la chanson : {current_song}")

            with open(current_song, 'rb') as f:
                bytes_per_second = 32000  # Assumons un débit de 256 kbps (32000 bytes/sec)
                elapsed_time = time.time() - song_start_time
                f.seek(int(elapsed_time * bytes_per_second))
                
                data = f.read(1024)
                while data:
                    yield data
                    data = f.read(1024)

            time.sleep(fade_duration)  # Ajouter un fondu entre les chansons
        else:
            time.sleep(1)
            print("Attente de la prochaine tranche horaire...")

@app.route('/stream')
def stream():
    return Response(generate(), mimetype='audio/mpeg')

@app.route('/playlists', methods=['GET', 'POST'])
def playlists():
    if request.method == 'POST':
        new_playlists = request.json
        save_playlists(new_playlists)
        global PLAYLISTS
        PLAYLISTS = new_playlists
        return jsonify({"message": "Playlists updated successfully!"})
    else:
        return jsonify(PLAYLISTS)

@app.route('/current_song', methods=['GET'])
def current_song_info():
    global current_song
    if current_song:
        song_title = os.path.basename(current_song)
        print(f"Envoi du titre de la chanson : {song_title}")
        return jsonify({"title": song_title})
    else:
        return jsonify({"title": "No song playing"})

if __name__ == '__main__':
    # Lancement automatique du serveur
    current_playlist_folder = get_playlist_folder()
    if current_playlist_folder is not None:
        songs = load_songs(current_playlist_folder)
        played_songs = set()
        current_song = get_next_song()
    print("Lancement du serveur de streaming...")
    app.run(host='0.0.0.0', port=8001, threaded=True)