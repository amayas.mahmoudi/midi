require('dotenv').config();

if (process.env.JWT_SECRET) {
  console.log('JWT_SECRET is set:', process.env.JWT_SECRET);
} else {
  console.log('JWT_SECRET is not set');
}
