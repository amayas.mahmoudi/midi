// backend/server.js
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const authRoutes = require('./routes/auth');
const adminRoutes = require('./routes/admin');
const app = express();
const PORT = 8002;

app.use(cors());
app.use(bodyParser.json());

app.use('/auth', authRoutes);
app.use('/admin', adminRoutes); // Ajoutez cette ligne pour inclure les routes admin

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
