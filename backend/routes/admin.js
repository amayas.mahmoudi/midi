const express = require('express');
const router = express.Router();
const authenticateToken = require('../middleware/auth');

router.get('/', authenticateToken, (req, res) => {
  res.json({ message: "Welcome to the admin area!" });
});

module.exports = router;
