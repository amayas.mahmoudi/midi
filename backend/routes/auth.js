const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');

const users = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../users.json')));

router.post('/login', (req, res) => {
  const { username, password } = req.body;
  const user = users.find(u => u.username === username && u.password === password);

  if (user) {
    const token = jwt.sign({ username: user.username, role: user.role }, process.env.JWT_SECRET, { expiresIn: '1h' });
    res.json({ authenticated: true, token });
  } else {
    res.json({ authenticated: false });
  }
});

module.exports = router;
