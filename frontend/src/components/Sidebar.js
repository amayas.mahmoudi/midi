import React, { useState } from 'react';
import './Sidebar.css';

const Sidebar = () => {
  const [isSidebarVisible, setSidebarVisible] = useState(false);

  const handleToggleSidebar = () => {
    setSidebarVisible(!isSidebarVisible);
  };

  return (
    <div className="sidebar-container">
      {!isSidebarVisible && (
        <button className="menu-button" onClick={handleToggleSidebar}>
          <div className="menu-icon">
            <div></div>
            <div></div>
            <div></div>
          </div>
        </button>
      )}
      {isSidebarVisible && (
        <div 
          className="sidebar"
          onMouseLeave={handleToggleSidebar}
        >
          <button className="sidebar-button" onClick={() => window.location.href = '/home'}>Home</button>
          <button className="sidebar-button" onClick={() => window.location.href = '/contact'}>Contact</button>
          <button className="sidebar-button" onClick={() => window.location.href = '/about'}>About</button>
        </div>
      )}
    </div>
  );
}

export default Sidebar;
