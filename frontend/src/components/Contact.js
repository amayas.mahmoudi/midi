import React, { useState } from 'react';
import './Contact.css';

const Contact = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    // Ici, vous pouvez ajouter la logique pour envoyer le formulaire au serveur.
    alert('Message envoyé!');
    setName('');
    setEmail('');
    setMessage('');
  };

  return (
    <div className="contact-form">
      <h2>Contactez-nous</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Nom
          
        </label>
        <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        <label>
          Email
          
        </label>
        <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        <label>
          Message
          
        </label>
        <textarea
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            required
          />
        <button type="submit">Envoyer</button>
      </form>
    </div>
  );
}

export default Contact;
