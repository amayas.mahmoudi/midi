// src/components/AudioContext.js
import React, { createContext, useState, useRef, useEffect } from 'react';

const AudioContext = createContext();

const AudioProvider = ({ children }) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [volume, setVolume] = useState(1);
  const [songTitle, setSongTitle] = useState("Chargement du titre...");
  const audioRef = useRef(new Audio());

  useEffect(() => {
    audioRef.current.volume = volume;
  }, [volume]);

  useEffect(() => {
    if (isPlaying) {
      audioRef.current.play();
    } else {
      audioRef.current.pause();
    }
  }, [isPlaying]);

  useEffect(() => {
    const fetchSongTitle = async () => {
      try {
        const response = await fetch('http://localhost:8001/current_song');
        const data = await response.json();
        const titleWithoutExtension = data.title.replace('.mp3', '');
        setSongTitle(titleWithoutExtension);
      } catch (error) {
        console.error("Erreur lors de la récupération du titre de la chanson :", error);
        setSongTitle("Erreur de chargement du titre");
      }
    };

    fetchSongTitle();

    const interval = setInterval(() => {
      if (isPlaying) {
        fetchSongTitle();
      }
    }, 5000);

    return () => clearInterval(interval);
  }, [isPlaying]);

  return (
    <AudioContext.Provider value={{ audioRef, isPlaying, setIsPlaying, volume, setVolume, songTitle }}>
      {children}
    </AudioContext.Provider>
  );
};

export { AudioContext, AudioProvider };
