// src/components/Playlists.js
import React, { useState } from 'react';
import './Playlists.css';

const Playlists = () => {
  const playlists = [
    { title: 'Playlist 1', image: 'https://images.pexels.com/photos/2417842/pexels-photo-2417842.jpeg?auto=compress&cs=tinysrgb&w=600' },
    { title: 'Playlist 2', image: 'https://images.pexels.com/photos/380782/pexels-photo-380782.jpeg?auto=compress&cs=tinysrgb&w=600' },
    { title: 'Playlist 3', image: 'https://images.pexels.com/photos/678725/pexels-photo-678725.jpeg?auto=compress&cs=tinysrgb&w=600' },
    { title: 'Playlist 4', image: 'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?auto=compress&cs=tinysrgb&w=600' },
    { title: 'Playlist 5', image: 'https://images.pexels.com/photos/417173/pexels-photo-417173.jpeg?auto=compress&cs=tinysrgb&w=600' }
  ];

  const [activeIndex, setActiveIndex] = useState(2);

  const handleScrollLeft = () => {
    setActiveIndex((prevIndex) => (prevIndex - 1 + playlists.length) % playlists.length);
  };

  const handleScrollRight = () => {
    setActiveIndex((prevIndex) => (prevIndex + 1) % playlists.length);
  };

  return (
    <div className="playlists-container">
      <div className="playlist" onClick={handleScrollLeft}>
        <img src={playlists[(activeIndex - 1 + playlists.length) % playlists.length].image} alt={playlists[(activeIndex - 1 + playlists.length) % playlists.length].title} className="playlist-image" />
        <div className="playlist-title">{playlists[(activeIndex - 1 + playlists.length) % playlists.length].title}</div>
      </div>
      <div className="playlist active">
        <img src={playlists[activeIndex].image} alt={playlists[activeIndex].title} className="playlist-image" />
        <div className="playlist-title">{playlists[activeIndex].title}</div>
      </div>
      <div className="playlist" onClick={handleScrollRight}>
        <img src={playlists[(activeIndex + 1) % playlists.length].image} alt={playlists[(activeIndex + 1) % playlists.length].title} className="playlist-image" />
        <div className="playlist-title">{playlists[(activeIndex + 1) % playlists.length].title}</div>
      </div>
    </div>
  );
}

export default Playlists;
