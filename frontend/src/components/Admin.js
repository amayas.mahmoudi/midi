// src/components/Admin.js
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import './Admin.css';
import { useAuth } from './AuthContext';

const Admin = () => {
  const navigate = useNavigate();
  const { logout } = useAuth();

  const fetchAdminData = async () => {
    const token = localStorage.getItem('token');
    try {
      console.log('Checking authentication for admin access, token:', token);
      const response = await axios.get('http://localhost:8002/admin', {
        headers: { Authorization: `Bearer ${token}` }
      });
      console.log('Admin data:', response.data);
    } catch (error) {
      console.error('Admin access denied:', error);
      logout();
      navigate('/login');
    }
  };

  useEffect(() => {
    fetchAdminData();
  }, []);

  return (
    <div>
      <h1>Admin Page</h1>
      <button onClick={logout}>Logout</button>
    </div>
  );
};

export default Admin;
