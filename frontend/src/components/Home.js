// src/components/Home.js
import React, { useState, useEffect, useRef, useCallback, useMemo, useContext } from 'react';
import './Home.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVolumeUp, faVolumeMute, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import Playlists from './Playlists';

function Home() {
  const [photos, setPhotos] = useState([]);
  const [isPlaying, setIsPlaying] = useState(false);
  const audioRef = useRef(null);
  const [volume, setVolume] = useState(1);
  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const [songTitle, setSongTitle] = useState("Chargement du titre...");

  const topics = useMemo(() => ['nature', 'technology', 'people', 'architecture', 'animals'], []);

  const fetchPhotos = useCallback(async () => {
    try {
      let allPhotos = [];
      for (let topic of topics) {
        const response = await fetch(`https://api.pexels.com/v1/search?query=${topic}&per_page=80`, {
          headers: {
            Authorization: 'E2f46dMEIvOLtGJ3dB7kyJRhUs6KIy2tZ0hSs3B5QyGXpXWB8YshFunx' // Remplacez par votre clé API Pexels
          }
        });
        const data = await response.json();
        allPhotos = allPhotos.concat(data.photos);
      }
      setPhotos(allPhotos);
    } catch (error) {
      console.error('Erreur lors de la récupération des photos :', error);
    }
  }, [topics]);

  const togglePlayPause = () => {
    if (!isPlaying) {
      audioRef.current.src = "http://localhost:8001/stream";
      audioRef.current.load();
      audioRef.current.play();
      setIsPlaying(true);
    } else {
      audioRef.current.pause();
      setIsPlaying(false);
    }
  };

  const toggleMute = () => {
    if (volume > 0) {
      setVolume(0);
    } else {
      setVolume(1);
    }
  };

  const increaseVolume = () => {
    setVolume(prevVolume => Math.min(prevVolume + 0.1, 1));
  };

  const decreaseVolume = () => {
    setVolume(prevVolume => Math.max(prevVolume - 0.1, 0));
  };

  useEffect(() => {
    if (audioRef.current) {
      audioRef.current.volume = volume;
    }
  }, [volume]);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentImageIndex(Math.floor(Math.random() * photos.length));
    }, 20000); // Change image every 20 seconds
    return () => clearInterval(interval);
  }, [photos.length]);

  useEffect(() => {
    const fetchSongTitle = async () => {
      try {
        const response = await fetch('http://localhost:8001/current_song');
        const data = await response.json();
        console.log("Réponse reçue :", data);
        const titleWithoutExtension = data.title.replace('.mp3', ''); // Supprimer l'extension .mp3
        setSongTitle(titleWithoutExtension);
      } catch (error) {
        console.error("Erreur lors de la récupération du titre de la chanson :", error);
        setSongTitle("Erreur de chargement du titre");
      }
    };

    fetchSongTitle();

    const interval = setInterval(() => {
      if (isPlaying) {
        fetchSongTitle();
      }
    }, 5000); // Mettre à jour toutes les 5 secondes
    return () => clearInterval(interval);
  }, [isPlaying]);

  useEffect(() => {
    fetchPhotos();
    const interval = setInterval(fetchPhotos, 600000); // Re-fetch photos every 10 minutes
    return () => clearInterval(interval);
  }, [fetchPhotos]);

  return (
    <div>
      <div className="overlay">
        <div className="header">
          <img src="/img/logo.png" alt="Logo Midi Radio" />
        </div>
        <div className="content">
          <div className="image-container">
            {photos.length > 0 && (
              <img src={photos[currentImageIndex].src.medium} alt="Current" className="fixed-size-image"/>
            )}
          </div>
          <div id="playlist-name" className="playlist-name">
            {songTitle}
          </div>
          <div className="center">
            <input type="checkbox" id="toggle" onChange={togglePlayPause} checked={isPlaying} />
            <label htmlFor="toggle" className="button">
              <div className="lines">
                <div className="line"></div>
                <div className="move">
                  <div className="line"></div>
                  <div className="line"></div>
                </div>
              </div>
            </label>
          </div>
          
          <div className="volume-control">
            <button className="volume-button" onClick={toggleMute} title="Mute/Unmute">
              <FontAwesomeIcon icon={volume > 0 ? faVolumeUp : faVolumeMute} />
            </button>
            <button className="volume-button" onClick={decreaseVolume} title="Decrease Volume">
              <FontAwesomeIcon icon={faMinus} />
            </button>
            <div className="volume-slider">
              <input
                type="range"
                id="volume"
                min="0"
                max="1"
                step="0.01"
                value={volume}
                onChange={(e) => setVolume(e.target.value)}
                title="Volume"
              />
            </div>
            <button className="volume-button" onClick={increaseVolume} title="Increase Volume">
              <FontAwesomeIcon icon={faPlus} />
            </button>
          </div>
        </div>
       
        <audio ref={audioRef} />
      </div>
      <br/>
      <h2>Playlists</h2>
      <Playlists />
      
    </div>
  );
}

export default Home;
