// src/components/About.js
import React from 'react';
import './About.css';

const About = () => {
  return (
    <div>
       <div className="footer">
            <img src="/img/logomidi.png" alt="Instagram" /> 
        </div>
      <h2>Notre Vision</h2>
      <p>
        Chez MIDI, nous créons des identités musicales uniques pour les marques et hôtels. 
        Notre webradio dynamique et personnalisable offre des playlists variées tout au long de la semaine.
      </p>
      <h2>Notre Mission</h2>
      <p>
        Nous fournissons des expériences musicales de haute qualité, renforçant l'identité des marques. 
        Notre webradio propose des playlists évolutives et une interface permettant d'intégrer logos et images de fond.
      </p>
      <div className="footer">
          <a href="https://www.instagram.com/midiradio" target="_blank" rel="noopener noreferrer">
            <img src="/img/logo_instagram.png" alt="Instagram" />
          </a>
          <a href="https://www.facebook.com/midiradio" target="_blank" rel="noopener noreferrer">
            <img src="/img/logo_facebook.png" alt="Facebook" />
          </a>
          
        </div>
    </div>
    
  );
}

export default About;
