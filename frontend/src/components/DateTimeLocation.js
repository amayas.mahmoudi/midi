// src/components/DateTimeLocation.js
import React, { useEffect, useState } from 'react';

const DateTimeLocation = () => {
  const [dateTime, setDateTime] = useState(new Date());

  useEffect(() => {
    const timer = setInterval(() => {
      setDateTime(new Date());
    }, 1000);

    return () => clearInterval(timer);
  }, []);

  const getLocation = () => {
    // This is a placeholder for location data.
    // In a real application, you might use an API to get the user's location.
    return 'Paris, France';
  };

  const getFormattedDate = (date) => {
    const options = {
      weekday: 'long',
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    };
    return new Intl.DateTimeFormat('fr-FR', options).format(date);
  };

  return (
    <div className="date-time-location">
      <div>{getFormattedDate(dateTime)}</div>
      <div>{getLocation()}</div>
    </div>
  );
}

export default DateTimeLocation;
